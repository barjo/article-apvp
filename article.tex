\documentclass[a4paper, twocolumn, DIV=calc, 10pt]{scrartcl}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc} 
\usepackage{charter}
\usepackage[francais]{babel} %texlive-lang-french
\usepackage{times}
\usepackage{listings}
\usepackage{graphicx,wrapfig}
\usepackage{tabularx, multirow}
\usepackage{algorithm,algpseudocode} %texlive-science
\usepackage{amsmath, amssymb, nicefrac}
%\usepackage[a4paper, vmargin=2cm, hmargin=2.5cm]{geometry}
\usepackage[font={small}]{caption}
\usepackage[hidelinks]{hyperref}
\KOMAoptions{DIV=last}
\title{Article AP}
\author{Joris \bsc{Barrier}\thanks{CNRS-LAAS, 7 avenue du Colonel
    Roche, F-31400 Toulouse, France joris.barrier@laas.fr}}

\begin{document}
  \maketitle
  \begin{abstract}
    Nous proposons un programme client-serveur de Retrait
    d'Information Privé modulaire et réutilisable accompagné d'une bibliothèque
    cryptographique capable de réaliser des calculs à
    plus d'un gigabit par secondes sur un processeur d'ordinateur de bureau.
  \end{abstract}

  \section{Introduction}
  Le Retrait d'Information Privé, abrégé RIP, est un ensemble 
  d'opérations qui permet de télécharger un élément d'une base de
  donnée publique hébergée sur un serveur sans que l'administrateur du
  serveur puisse déterminer quel élément est récupéré. Trivialement,
  télécharger l'intégralité des données présentes sur la base garantie
  un tel niveau de confidentialité. L'objectif principal du RIP est de
  conserver le même niveau de confidentialité tout en réduisant les
  coûts de communication.

  L'utilisation du RIP peut par exemple avoir un intérêt dans l'utilisation d'une
  bibliothèque publique informatisée. Un utilisateur de la
  bibliothèque pourrait accéder à différents types de publications
  sans que ses centres d'intérêts soient dévoilés. Une plateforme de
  voix sur IP basé sur un protocole RIP permettrait de garantir la
  confidentialité des discussions mais aussi des interlocuteurs. 

  Le RIP est historiquement introduit par Chor, Goldreich, Kushilevitz
  et Sudan \cite{PIRorig} en 1995.
  L'architecture des différents protocoles de RIP qu'ils proposent
  comporte plusieurs serveurs hébergeant chacun une réplique de la
  base de données.
  Cependant Kushilevitz et Ostrovsky \cite{KO97}, montrent en 1997 qu'effectuer
  un RIP symétrique, \emph{i.e.} un unique élément est récupéré
  sur un unique serveur, est possible en utilisant un problème réputé 
  difficile basé sur le résidu quadratique.
  En 1998 Stern \cite{S98}, propose un protocole permettant de
  télécharger un élément de taille arbitraire en utilisant le 
  crypto-système de Paillier \cite{P99}. De nombreux autres modèles de
  RIP utilisant la théorie des nombres naissent ensuite \cite{CMS99}, appelés
  \emph{computationally} RIP.

  En 2007 Sion et Cabunar \cite{SC07} coupent l'engouement pour le RIP
  symétrique. En effet, ils montrent que télécharger l'intégralité de
  la base de donnée est toujours plus rapide que d'effectuer un RIP
  basé sur des primitives cryptographiques en utilisant du matériel
  informatique non spécialisé. Leur approche utilise le modèle proposé par Kushilevitz
  Ostrovsky \cite{KO97} qui nécessite une multiplication modulaire par
  bit de le base de donnée.

  Les multiplications modulaires étant trop couteuses en terme de
  ressources processeur, le RIP renait à partir de nouvelles
  primitives cryptographiques, dont les coût calculatoire sont plus
  faibles, basées sur les réseaux algébriques. 
  Les premières constructions, basées sur une variante de NTRU \cite{HPS98}, sont
  proposées par Alguilar-Melchor et Gaborit \cite{AG07,FastPIR}.
  Le bénéfice d'une telle construction face à la solution triviale est
  montré par Olumofin et Golberg \cite{OG11}.

  Notre contribution est un programme client/serveur résolument modulaire de retrait
  d'information privé muni d'une bibliothèque cryptographique 
  proposant NTRU, Ring-LWE \cite{R05} (une autre construction basée sur les réseaux)
  et le chiffrement de Paillier ainsi qu'un optimiseur capable 
  de donner un ensemble de paramètres cryptographique et RIP selon une
  situation donnée.
  
 
  \section{Outils}
  \subsection{Chiffrement Homomorphe}
  Les algorithmes  des crypto-systèmes homomorphes utilisés pour le RIP, sont
  composés de quatre fonctions principales :
  \begin{itemize}
  \item   \texttt{KeyGen} qui, selon certains paramètres de sécurité, génère
  les clés.  
  \item \texttt{Enc} la fonction de chiffrement, prenant en argument
    \emph{pk} la clé publique du système et \emph{m} les données à
    chiffrer. Afin d'assurer sa sécurité, un tel crypto-système doit être
    \emph{randomized} c'est à dire que, lors du processus de chiffrement, des valeurs
    aléatoires sont introduites, ainsi, pour un couple (\emph{pk, a})
    un grand nombre de chiffrés peuvent correspondre.
  \item \texttt{Dec} la fonction de déchiffrement, prenant en
    argument \emph{sk} la clé secrète du système et \emph{c} les donnés à déchiffrer.
  \item \texttt{Hom} l'ensemble des fonctions que le
    crypto-système peut évaluer sur des données chiffrées. Certains systèmes
    comme le chiffrement de Paillier et LWE sont capables d'additionner des
    chiffrés alors que d'autres, comme El Gamal, donnent la
    possibilité de les multiplier. Plus formellement, soit deux
    anneaux A et B l'application $f : A  \rightarrow B $ qui vérifie
    $\forall a,b \in A, f(a + b) = f(a) + f(b)$ et $f(a \times b) = f(a) \times f(b)$ est appelé homomorphisme.

 %En 2007, Craig Gentry
    %propose un crypto-système entièrement homomorphe, donnant la
    %possibilité d'évaluer l'addition et la multiplication sur des
    %chiffrés. Cependant, un tel système étant très couteux en terme de
    %temps processeur, il n'est pas utilisable en l'état pour le RIP.
  \end{itemize}
  \vspace{0.2cm}
  Notre bibliothèque, réutilisable en dehors de notre système de RIP, propose le chiffrement de Paillier,
  Ring-LWE basé sur \cite{BV11} qui est une version symétrique de LWE
  et NTRU auxquels nous avons adjoint des opérations de pré-calcul
  (voir \ref{lab:ntttools}) afin d'améliorer leurs performances lors
  des opérations de RIP. 
  De plus,les différentes interfaces proposées permettent d'ajouter aisément
  de nouveaux crypto-systèmes.

  \subsection{RIP}
  Le Retrait d'information Privé utilisé dans notre modèle est très proche de
  \cite{KO97}. Soit une base de donnée de $n$ éléments de $\ell-$bits
  chacun disponible sur un serveur.

  En premier lieu, le client reçoit la liste des éléments
  disponibles sur le serveur. 
  Il choisit le fichier d'index $i$ pour lequel il fait correspondre
  un chiffré de un, pour les autres il génère des chiffrés de zero. 
  L'ensemble obetenu, appelée requête RIP, est intégralement envoyé au
  serveur. Une fois la requête reçue par le serveur, ce dernier combine, via une
  opération dépendant du crypto-système utilisé, chaque élément de la
  requête à l'élément de la base de donnée correspondant. Ainsi les
  éléments auxquels sont associés les chiffrés de zéro deviennent des
  chiffrés de zéro et le $i-$ème élément auquel est associé le chiffré
  de un devient un chiffré du $i-$ème élément. Ensuite, le serveur
  combine les différents chiffrés obtenus, se rapportant, si les
  données étaient clair à la somme du  $i-$ème élément et de zéro.
  Une fois la réponse obtenue, elle est envoyée au client qui la déchiffre
  et obtient le $i-$ème élément choisit au départ.

  Notons que les éléments peuvent être segmentés s'ils ne sont pas
  représentable dans un seul chiffré. En effet, un chiffré ne peut contenir
  qu'une quantité limitée d'information. Dans ces conditions, la
  réponse est calculée comme précédemment pour chaque segment. Le
  client déchiffre chaque segment et les assemble afin d'obtenir
  l'élément choisi.

  Dans ce modèle le nombre d'éléments de la requête est $n$, ainsi la
  taille de la requête est en $O(n)$. Soit $E$ le facteur
  d'expansion du crypto-système utilisé, la taille de la réponse
  envoyée par le serveur est de $E.\ell$ bits. Cependant, un procédé
  permet de réduire la taille de la requête envoyée par le client. \label{recursion}
  Le principe est de transformer la base de données de $n$ éléments en
  $\sqrt{n}$ sous-bases de données. Supposons que le client souhaite
  télécharger le $j-$ème élément de la $i-$ème sous-base de données. Il calcul une
  première partie de requête de $\sqrt{n}$ chiffrés afin de conserver seulement le
  $j-$ème élément de chacunes des base de données. Le serveur effectue l'équivalent d'un RIP
  dont le résultat est $\sqrt{n}$ chiffrés.
  Puis, le client calcule\footnote{Le client peut aussi calculer les
    deux parties de la requête et les envoyer en une seule fois.} 
  la seconde partie de la requête afin de sélectionner le $i-$ème
  élément de la sous base de donnée qui a été conservé lors de
  la première opération de RIP. Le serveur applique cette requête sur
  la seconde base de données chiffrée obtenue précédemment et obtient un
  unique chiffré du $j-$ème élément de la $i-$ème sous
  base de donnée. Le client déchiffre la réponse reçue une première
  fois pour obtenir le $j-$ème élément chiffré de la base de données
  temporaire et, une seconde fois, pour obtenir l'élément choisit.

  Ce procédé peut être généralisé à $d$ niveau de récursion, ainsi la
  taille de la requête est en $O(dn^{\nicefrac{1}{d}})$ et la taille
  de la réponse de $E.\ell^d$ bits.
  
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=9cm]{Images/pir_mux.pdf}
    \caption{Exemple de RIP sur 9 éléments}
    \label{pir3x3}
  \end{figure}

  La figure \ref{pir3x3}  est un exemple de RIP à deux dimension pour
  une base de données de neufs éléments segmentés en trois sous-bases
  de données de 3 éléments chacune. Le client cherche à télécharger
  l'élément de valeur huit qui est, comme on peut le voir sur la
  figure \ref{pir3x3}, le second élément de la troisième
  sous-base de données. La première partie de la requête permet de
  générer la base intermédiaire composée des chiffrés du second
  élément de chacune des trois sous-bases, c'est à dire, d'un chiffré
  de deux, d'un chiffré de 5 et d'un chiffré de 8. La seconde partie de la
  requête permet de sélectionner le troisième élément de la base
  temporaire. Le résultat obtenu est le chiffré d'un chiffré de
  l'élément de valeur huit que le client doit déchiffrer deux fois.
  
  
%%%%% FIN OUTILS %%%%
  \section{Proposition}
  Le programme présenté est entièrement écrit dans le langage
  \texttt{C++}. Certaines fonctionnalités sont fournies par la
  bibliothèque \texttt{boost}
  \subsection{Architecture Générale}
  L'architecture générale du programme, est composé de quatre unités
  fonctionnelles distincts, trois sont empilés et articulées par des
  interfaces.

  La première, appelée \emph{Facade client-serveur} dans la figure
  \ref{fig:archi} est responsable des fonctionnalités de communications
  necessaire aux RIP ainsi qu'aux interactions utilisateur. 
  

  La seconde, appelée \emph{PIR}
  représente l'ensemble des fonctions de communications et
  d'interactions entre le client et le serveur ainsi que les fonctions
  effectuant les opérations de RIP à proprement parler.

  La troisième, nommée \emph{Hommomorphic Crypto}, contient l'ensemble
  des crypto-systèmes disponibles (le chiffrement de Paillier, LWE,
  NTRU) munis des fonctions particulière au
  RIP. Le premier module accède au second par un ensemble de primitives
  définies par \emph{Homomorphic Crypto Interface}.

  Le dernier bloc, du coté gauche de la figure, 
  représente l'optimiseur qui effectue des
  opérations de PIR ainsi que des opérations de plus bas niveau sur
  les différents crypto-systèmes.
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=9cm]{Images/global-archi-detail.pdf}
    \caption{Architecture générale.}
    \label{fig:archi}
  \end{figure}

  \subsection{Module Cryptographique}
  Le module cryptographique est composé de trois crypto-systèmes
  différents.
  Deux sont basés sur les réseaux géométriques, NTTLWE qui est une
  version symétrique de Ring-LWE \cite{LPR10} et NTTNTRU basé sur
  version de NTRU dont les paramètres de sécurité ont été revus
  \cite{S13}.
  Le chiffrement de Paillier, basé sur la théorie des nombres, est
  implémenté à des fins comparatives.

  On voit dans la figure \ref{fig:crypto} que les crypto-systèmes
  s'appuient sur trois bibliothèques. GMP et Flint sont deux
  bibliothèques connues pour leurs performances sur les entiers de
  taille arbitraire. NTTTools, détaillé dans la sous-section suivante,
  est notre contribution majeure dans le module cryptographique.
  C'est un ensemble de fonctions permettant de modéliser et
  d'effectuer des calculs sur les réseaux géométriques.

  Les constructions cryptographique présentées sont indépendantes et peuvent être
  utilisées en dehors de notre programme.

  \begin{figure}[!ht]
    \centering
    \includegraphics[width=8cm]{Images/ntt.pdf}
    \caption{Module Cryptographique}
    \label{fig:crypto}
  \end{figure}

  \subsection{NTTTools}\label{lab:ntttools}

  Les réseaux utilisés dans Ring-LWE ou NTRU, sont composés de
  polynômes de degrés $d$ dont les coefficients sont représentés modulo un
  entier $p$. Les additions et les multiplications entre polynômes sont
  effectuées modulo le polynôme $X^d + 1$.

  Ces opérations sont utilisées intensivement lors du calcul de la réponse du
  serveur PIR et représentent un goulot d'étranglement pour les performances. En effet la
  multiplication de polynômes naïve est en $O(n^2)$. Nous proposons
  d'améliorer cette borne en combinant trois méthodes.
  \begin{itemize}
  \item Le théorème des restes chinois est utilisé pour effectuer des
    opérations avec des modules de soixante bits pour conserver de
    hautes performances lors des opérations de coefficients à
    coefficients. Le principe est le suivant, les coefficients d'un
    polynôme, au lieu d'être représentés modulo un grand entier de plus de
    60, sont représenté par $m$ polynômes dont les coefficients sont
    représentés par un module de 60 bits. 
    La réponse RIP est recombinée par le client une fois tous les 
    polynômes déchiffrés.
  \item La méthode de multiplication modulaire de Shoup. Soit deux
    entier $a$ et $b$ de 64 bits et leur module $p$. Multiplier $a$
    par $b$ modulo revient a précalculer $b'=b/p$ puis 
    
    \begin{eqnarray}
      \label{eq:1}
      q =& ab' / 2^{64} \\
      r =& ab - qp \mod{2^{64}} 
    \end{eqnarray}
  \item La transformé de Fourrier discrète sur les entiers modulo
    $p$. (le corps fini $\nicefrac{\mathbb{Z}}{p\mathbb{Z}}$). Les
    séquences de chiffres que représentent les coefficients sont
    interprétées comme les éléments d'un vecteur dont on calcule les
    convolutions. Ainsi pour multiplier deux polynômes, on multiplie
    leur TFD entre elles puis la TFD du résultat est inversée.
    Dans notre modèle, les TFD sont réalisés comme pré-calcul sur la
    base de donnée, et lors de la réception des éléments de requête du
    client. 
    Les TFD inverses sont effectués par le client durant le processus
    de déchiffrement de la réponse.
 
  \end{itemize}

  \subsection{Module RIP}
  Le module RIP est en deux parties. Le client est composé du
  générateur de requête qui crée les chiffrés de zéro et un, et de
  l'extracteur de réponse qui déchiffre et réassemble la réponse
  envoyée par le serveur. La partie serveur est composée de deux
  générateurs de réponse, le premier est réservé au
  chiffrement de Paillier car utilisant la bibliothèque \texttt{GMP}
  et un second utilisable par \texttt{NTTLWE} et \texttt{NTTNTRU} car
  utilisant \texttt{NTTTools}. 

  Ces deux modules de réponse proposent les même primitives 
  et fonctionnalités par un mécanisme d'héritage.
  Ainsi ils sont tous deux capables d'effectuer des RIP récursifs,
  d'importer les données et de les agréger.

  L'agrégation des éléments de la base consiste à les concaténer $\alpha$
  par $\alpha$, de cette manière le nombre d'éléments $n'=
  n / \alpha$. L'intérêt de cette pratique est discuté dans
  la sous-section \ref{lab:optim}.
  
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=9cm]{Images/pir.pdf}
    \caption{Module RIP}
    \label{fig:rip}
  \end{figure}
  
  \subsection{Optimiseur}
  L'optimiseur de paramètre RIP calcule des paramètres PIR suivant
  l'une des trois règles de minimisation possible :
  \begin{enumerate}
  \item le temps d'exécution complet du RIP pour un client
    récupérant un fichier;
  \item l'utilisation des ressources, c'est à dire le temps
    d'exécution sur le client, le serveur et le temps d'occupation de
    la bande passante.
  \item le cout d'utilisation du serveur s'il était hébergé dans le
    cloud, qui implique le cout d'utilisation d'une machine quatre
    coeur et de la bande passante. 
  \end{enumerate}

  L'espace à explorer est déterminé par un ensemble de paramètres fixé
  qui sont les différentes vitesses d'upload et de download du client et
  du serveur. (L'optimiseur peut réaliser un test de débit).
  Ainsi que le nombre d'éléments présents dans la base ainsi que la
  taille $\ell$ maximum de ces éléments.
  Et pour finir, le niveau de sécurité minimum requis (80 bits par exemple).
  Une fois l'espace défini le programme explore linéairement
  les paramètres PIR suivant:
  \begin{itemize}
  \item les crypto-systèmes et leurs paramètres en respectant
    $k$.
  \item $d$ le nombre de récusions. Correspond au nombre de sous-base
    de données qui devront être traitées récursivement (voire \ref{recursion}).
  \item $\alpha$ le nombre d'éléments agrégés. Cette zone de l'espace
    pouvant être particulièrement étendu, $\alpha$ l'optimiseur
    effectue une dichotomie car les modifications entrainées sont
    facilement prévisible ce qui n'est pas le cas des autres
    variables. Lorsque la taille de l'espace est suffisamment réduite,
    les valeurs d'$\alpha$ sont itérées.  
    Pour les crypto-systèmes basés sur les réseaux le nombre d'éléments peut
    influer rétrospectivement sur les paramètres cryptographiques.
  \end{itemize}

  \paragraph{} L'optimiseur fonctionne aussi en mode client-serveur. Le
  client envoie des requêtes au serveur afin d'obtenir la durée
  d'absorption et d'importation d'un bit pour un crypto-système donné.
  Notons que le serveur bénéficie d'un cache, lors du premier
  lancement ce dernier calcul l'ensemble des valeurs d'absorption et
  d'importation afin de répondre au plus vite aux demandes des clients.
  De la même manière, la partie cliente de l'optimiseur calcule
  un cache pour les durées de chiffrement et de déchiffrement afin de
  réduire, par la suite, les opérations d'optimisation des paramètres RIP. 

  

  \label{lab:optim}

  \section{Utilisation}
  \subsection{Client-Serveur}
  Le couple client-serveur possède plusieurs modes de
  fonctionnement. Le premier mode est nommé \emph{client driven} c'est-à
  -dire que les informations de PIR sont envoyées par le client au
  serveur. Dans second mode appelé \emph{server driven}, le serveur
  envoie les informations de RIP au client. 
  Cependant, le mode \emph{server driven} nécessite une session de configuration.

  \paragraph{Client driven}Dans ce mode, le serveur est directement
  démarré sans paramètres et attend la connexions des clients. Chaque
  client peut avoir des paramètres différents (crypto-système,
  agrégation, niveau de récursion\dots) impliquant d'importer la base
  de donnée pour chaque client.

  \paragraph{Server driven}Dans ce mode, le serveur envoie les
  informations relative au PIR au client après un premier cycle de
  configuration. Lors de cette session, les paramètres sont envoyés par
  le client (potentiellement calculé par l'optimiseur) et enregistrés
  par le serveur. Ainsi, tous les clients suivant bénéficieront de ces
  paramètres. Cette méthode permet d'importer la base de donnée une
  seule fois et donne de meilleurs résultats pour le temps total
  d'exécution de RIP.

  \paragraph{}Il est possible de donner au client de nombreux paramètres tel que
  le crypto-système avec ses paramètres, le niveau de récursion,
  l'agrégation.
  
  \subsection{Optimiseur}
  L'optimiseur à pour rôle de donner un ensemble de paramètre RIP
  suivant une règle d'optimisation et de paramètres
  donnés. L'optimiseur peut être utilisé indépendamment sur le client,
  ce mode est appelé \emph{dry run}. Une fois le spécifié, l'utilisateur peut donner en paramètre les variables citées
  dans la partie \ref{lab:optim}. De plus, l'outil peut
  considérer ou non le temps d'importation de la base de donnée durant
  le processus d'optimisation. 

  Il peut fonctionner avec ou sans
  serveur d'optimisation\footnote{La partie serveur de l'optimiseur
    se démarre à part sur un serveur local ou distant.}.
  Sans le serveur les durées relative au opérations homomorphes sont
  estimées. Avec le serveur, ces données, précédemment calculées lors
  de la phase de cache, sont directement envoyées au client à la demande.

  

  \section{Conclusion}
  
  
  
\bibliographystyle{plain}
\bibliography{bibliographie}

\end{document}

